#pragma once
#include <fstream>
#include "WordCounter.h"
#include"Parser.h"
#include"SortMapa.h"
#include "Reader.h"
#include"Writer.h"
namespace Kochetkov
{
	using std::string;
	
	WordCounter::WordCounter(const string& inputFileName, const string& outputFileName)
		:inputFileName(inputFileName), outputFileName(outputFileName)
	{	
	}

	void WordCounter::Run()
	{
		std::vector <std::string> text;
		std::map<std::string, int> word_map;
		std::vector<std::pair<std::string, int>> word_vector; 


		Reader reader(inputFileName);
		Parser parser(word_map);
		Writer writer(outputFileName);
		SortMapa sortmapa(word_vector);

		reader.ReatTextAndFillVector(text);
		parser.parseAndFillMap(text);
		int counter_all_words=sortmapa.Sort(word_map);
		writer.Write_out(word_vector, counter_all_words);
	}


}