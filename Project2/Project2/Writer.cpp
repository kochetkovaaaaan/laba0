#include "Writer.h"

namespace Kochetkov {
	const char precent_symbol = '%';
	const std::string CSVSEPAR = "; ";
	Kochetkov::Writer::~Writer()
	{
		output_file.close();
	}

	Kochetkov::Writer::Writer(const std::string& outputFile)
		:output_file(outputFile)
	{
	}

	void Writer::Write_out(const std::vector<std::pair<std::string, int>>& word_vector, int word_count)
	{
		for (auto p : word_vector) {
			output_file << p.first << CSVSEPAR << p.second << CSVSEPAR << (((double)p.second / word_count) * 100) << precent_symbol << std::endl;
		}
	}

}
