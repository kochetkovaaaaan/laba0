#pragma once
#include<fstream>
namespace Kochetkov
{
	using std::string;
	class WordCounter
	{
	public:
		WordCounter(const string &inputFileName, const string &outputFileName);
		void Run();
	private:
		const string inputFileName, outputFileName;
		
	};
	
}
