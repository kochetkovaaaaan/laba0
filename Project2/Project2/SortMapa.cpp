#include "SortMapa.h"
#include <algorithm>

namespace Kochetkov {
	Kochetkov::SortMapa::SortMapa(std::vector<std::pair<std::string, int>>& word_vector)
		:word_vector(word_vector)
	{
	}

	bool comparator(std::pair<std::string, int> a, std::pair<std::string, int> b) // ��� ���������� ���� �� ������� ������������� 
	{
		return a.second > b.second;
	}


	int Kochetkov::SortMapa::Sort(const std::map<std::string, int>& word_map)
	{
		int word_count=0;
		for (auto p : word_map)
		{
			word_count += p.second;
			word_vector.push_back(p);
		}
		std::sort(word_vector.begin(), word_vector.end(), comparator); 
		return word_count;
	}

}