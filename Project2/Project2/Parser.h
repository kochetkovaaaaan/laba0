#pragma once
#include <map>
#include <string>
#include<vector>
namespace Kochetkov
{
	class Parser
	{
	public:
		Parser(std::map<std::string, int>& word_map);
		void parseAndFillMap( const std::vector<std::string> &text);
	private:
		std::map<std::string, int> & word_map;
		 bool Separator(char a) const;
	};

}