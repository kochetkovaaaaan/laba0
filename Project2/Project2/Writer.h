#pragma once
#include<fstream>
#include<iostream>
#include<vector>
#include<string>
namespace Kochetkov {

	class Writer
	{
	public:
		virtual ~Writer();
		Writer(const std::string& outputFile);
		void Write_out(const std::vector<std::pair<std::string, int>>& word_vector,int word_count);

	private:
		std::ofstream output_file;
	};

}

